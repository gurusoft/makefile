#########################################
#   Makefile to assist the development  #
#########################################

include color.mk

VERSION="0.1.23"

DOCKER=docker
COMPOSE=docker-compose
DOCKERFILE=Dockerfile
COMPOSEFILE=docker-compose.yml

PROXY=proxy

%:
	@:

all: build

.docker_version:
	@$(DOCKER) -v
.compose_version:
	@$(COMPOSE) -v
.makefile_version:
	@echo $(MAKEFILE_LIST) $(Blue)$(VERSION)$(Color_Off)
.dockerfile_version:
	@test -f $(DOCKERFILE) && sed -n 's/\(^LABEL\ *version=\)"\(v\?[0-9]\+.[0-9]\+.[0-9]\+\)"/\2/Ip' $(DOCKERFILE) ||:

version: .dockerfile_version .makefile_version .docker_version .compose_version
	@echo version $(filter-out $@,$(MAKECMDGOALS))

up:
	@$(COMPOSE) -f $(COMPOSEFILE) up $(filter-out $@,$(MAKECMDGOALS))
	-@$(DOCKER) kill --signal HUP $(PROXY)

upd:
	@$(COMPOSE) -f $(COMPOSEFILE) up -d $(filter-out $@,$(MAKECMDGOALS))
	@$(DOCKER) kill --signal HUP $(PROXY)
down:
	@$(COMPOSE) -f $(COMPOSEFILE) stop $(filter-out $@,$(MAKECMDGOALS))
	@$(DOCKER) kill --signal HUP $(PROXY)
build:
	@$(COMPOSE) -f $(COMPOSEFILE) build --no-cache $(filter-out $@,$(MAKECMDGOALS))
pull:
	@$(COMPOSE) -f $(COMPOSEFILE) pull --ignore-pull-failures $(filter-out $@,$(MAKECMDGOALS))
